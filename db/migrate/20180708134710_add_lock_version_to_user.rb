class AddLockVersionToUser < ActiveRecord::Migration[5.0]
  def change
    # Add lock_version column to let Rails enable Optimistic lock on User model
    add_column :users, :lock_version, :integer, null: false, default: 0
  end
end
