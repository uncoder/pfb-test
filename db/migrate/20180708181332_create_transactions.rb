class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    # Create table to log transactions
    create_table :transactions do |t|
      t.references :user
      t.integer :version, null: false
      t.integer :amount, null: false, default: 0
      t.integer :balance, null: false, default: 0
      t.string :reason, null: false
      t.timestamps
    end

    add_foreign_key :transactions, :users, on_delete: :cascade
    add_index :transactions, [:user_id, :version], unique: true # set uniq index
  end
end
