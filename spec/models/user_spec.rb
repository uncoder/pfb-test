require 'rails_helper'

describe User do
  subject { FactoryGirl.build :user }

  it { is_expected.to be_valid }

  describe :attributes do
    it { is_expected.to respond_to :username }
    it { is_expected.to respond_to :email }
    it { is_expected.to respond_to :coins }
  end

  let :user { FactoryGirl.create :user, coins: 50 }

  describe "#give_coins" do
    it "increases coins amount" do
      user.give_coins(amount: 10, reason: 'test')
      expect(user.reload.coins).to eq(60)
    end

    it "raises exception" do
      expect {
        user.give_coins(amount: -10, reason: 'test')
      }.to raise_exception(Exception)
      expect(user.reload.coins).to eq(50)
    end

    it "calls add_coins" do
      expect(user).to receive(:add_coins).with(10, 'test')
      user.give_coins(amount: 10, reason: 'test')
    end

    it "calls retry_if_locked" do
      expect(user).to receive(:retry_if_locked)
      user.give_coins(amount: 10, reason: 'test')
    end
  end

  describe "#take_coins" do
    it "decreases coins amount" do
      user.take_coins(amount: 10, reason: 'test')
      expect(user.reload.coins).to eq(40)
    end

    it "raises exception" do
      expect {
        user.take_coins(amount: -10, reason: 'test')
      }.to raise_exception(Exception)
      expect(user.reload.coins).to eq(50)
    end

    it "calls add_coins" do
      expect(user).to receive(:add_coins).with(-10, 'test')
      user.take_coins(amount: 10, reason: 'test')
    end

    it "calls retry_if_locked" do
      expect(user).to receive(:retry_if_locked)
      user.give_coins(amount: 10, reason: 'test')
    end
  end

  describe "#take_all_coins" do
    it "resets coins amount" do
      user.take_all_coins(reason: 'test')
      expect(user.reload.coins).to eq(0)
    end

    it "calls reset_coins" do
      expect(user).to receive(:reset_coins).with('test')
      user.take_all_coins(reason: 'test')
    end

    it "calls retry_if_locked" do
      expect(user).to receive(:retry_if_locked)
      user.take_all_coins(reason: 'test')
    end
  end

  describe "#take_prize" do
    context "when the user has enough coins" do
      let :prize { FactoryGirl.create :prize, coins: 30, name: 'prize' }

      it "decrease coins amount" do
        user.take_prize(prize: prize)
        expect(user.reload.coins).to eq(20)
      end

      it "calls add_coins" do
        expect(user).to receive(:add_coins).with(-30, 'Prize: prize')
        user.take_prize(prize: prize)
      end

      it "calls retry_if_locked" do
        expect(user).to receive(:retry_if_locked)
        user.take_prize(prize: prize)
      end
    end

    context "when the user has not enough coins" do
      let :prize { FactoryGirl.create :prize, coins: 100, name: 'prize' }

      it "raises exception" do
        expect {
          user.take_prize(prize: prize)
        }.to raise_exception(Exception)
        expect(user.reload.coins).to eq(50)
      end
    end
  end
end
