require 'rails_helper'

describe User do
  subject { FactoryGirl.create :user, coins: 50 }

  it "responds to transactions" do
    subject.respond_to?(:transactions)
  end

  describe "#add_coins" do
    context "when amount is positive" do
      it "adds coins" do
        subject.send(:add_coins, 10, 'test')
        expect(subject.reload.coins).to eq(60)
      end
    end

    context "when amount is negative" do
      it "subtracts coins" do
        subject.send(:add_coins, -10, 'test')
        expect(subject.reload.coins).to eq(40)
      end
    end

    it "calls track_amounts" do
      expect(subject).to receive(:track_amounts)
      subject.send(:add_coins, 10, 'test')
    end
  end

  describe "#reset_coins" do
    it "resets coins" do
      subject.send(:reset_coins, 'test')
      expect(subject.reload.coins).to eq(0)
    end

    it "calls track_amounts" do
      expect(subject).to receive(:track_amounts)
      subject.send(:reset_coins, 'test')
    end
  end

  describe "#track_amounts" do
    let :update { lambda { subject.update(coins: 90) } }

    it "saves record" do
      expect {
        subject.send(:track_amounts, 'reason', &update)
      }.to change {
        Transaction.count
      }.by(1)
    end

    context "saves" do
      before do
        subject.send(:track_amounts, 'reason', &update)
      end

      let :transaction { Transaction.last }

      it "user_id" do
        expect(transaction.user_id).to eq(subject.id)
      end

      it "version" do
        expect(transaction.version).to eq(subject.lock_version)
      end

      it "amount" do
        expect(transaction.amount).to eq(40)
      end

      it "balance" do
        expect(transaction.balance).to eq(90)
      end

      it "reason" do
        expect(transaction.reason).to eq('reason')
      end
    end
  end

  describe "#retry_if_locked" do
    it "retries block 5 times" do
      allow(subject).to receive(:add_coins).with(1, 'test') do
        raise ActiveRecord::StaleObjectError
      end

      expect(subject).to receive(:reload).exactly(5).times
      expect(subject).to receive(:add_coins).with( 1, 'test').exactly(6).times

      l = lambda { subject.send(:add_coins, 1, 'test') }

      expect {
        subject.send(:retry_if_locked, &l)
      }.to raise_exception(ActiveRecord::StaleObjectError)
    end
  end

  # Stale issue testing
  describe "concurrent writing" do
    let :user_instances do
      5.times.map do |i|
        User.find(subject.id)
      end
    end

    it "raises an exception" do
      expect {
        user_instances.each { |u| u.send(:add_coins, 1, 'some-reason') }
      }.to raise_exception(ActiveRecord::StaleObjectError)
    end

    context "with_retries" do
      it "keeps consistency" do
        l = lambda { subject.send(:add_coins, 1, 'test') }

        user_instances.each { |u| u.send(:retry_if_locked, &l) }
        expect(subject.reload.coins).to eq(55)
      end
    end
  end
end
