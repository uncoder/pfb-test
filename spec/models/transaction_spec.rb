require 'rails_helper'

describe Transaction, type: :model do
  subject { FactoryGirl.build :transaction }

  it { is_expected.to be_valid }

  describe :attributes do
    it { is_expected.to respond_to :user }
    it { is_expected.to respond_to :version }
    it { is_expected.to respond_to :amount }
    it { is_expected.to respond_to :balance }
    it { is_expected.to respond_to :reason }
  end
end
