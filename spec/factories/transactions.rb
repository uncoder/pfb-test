FactoryGirl.define do
  factory :transaction do
    user
    version 1
    amount 50
    balance 0
    reason "Reason"
  end
end
