class Transaction < ApplicationRecord
  belongs_to :user

  validates :user_id, presence: true
  validates :version, presence: true, uniqueness: { scope: [:user_id] }
  validates :amount, presence: true, numericality: true
  validates :balance, presence: true

  scope :ordered, -> { order(version: :asc) }
end
