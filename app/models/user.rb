class User < ApplicationRecord
  include HasCoins # All magic is there

  validates   :username, uniqueness: true
  validates   :email, length: { in: 8..100 },
    format: { with: /\A([a-z0-9\-_\.]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}, on: :create
  validates :level, presence: true, numericality: { greater_than: -1 }
  validates :coins, presence: true, numericality: { greater_than: -1 }

  def give_coins(amount:, reason:)
    raise 'Cannot add negative amount' if amount < 0

    retry_if_locked { add_coins(amount, reason) }
  end

  def take_coins(amount:, reason:)
    raise 'Cannot subtract negative amount' if amount < 0

    retry_if_locked { add_coins(-amount, reason) }
  end

  def take_all_coins(reason:)
    retry_if_locked { reset_coins(reason) }
  end

  # Additional method to take prize. Uses take_coins method
  def take_prize(prize:)
    raise 'Not enough coins' if prize.coins > coins

    reason = "Prize: #{prize.name}"
    return unless take_coins(amount: prize.coins, reason: reason)
    # user.log_prize(orize)
    # or any other code here
  end
end
