class User
  module HasCoins
    extend ActiveSupport::Concern

    RETRIES_IF_LOCKED = 5

    included do
      has_many :transactions, dependent: :destroy
    end

    private

    def add_coins(amount, reason)
      self.coins += amount

      track_amounts(reason) { save }
    end

    def reset_coins(reason)
      self.coins = 0

      track_amounts(reason) { save }
    end

    # Tracks every change of coins assuming :add_coins and :reset_coins
    # are only entry-point to change coins amount
    def track_amounts(reason)
      old_coins = coins_was
      return unless yield

      # Enqueue a worker to save transaction, it's faster than writting to db
      # :blance is a snaphot after change
      # Jobs can be performed in random order, so :lock_version helps to order transactions properly
      LogTransactionsJob.perform_later(
        user: self,
        version: lock_version,
        amount: coins - old_coins,
        balance: coins,
        reason: reason,
        created_at: Time.zone.now.to_s
      )
    end

    # Since we are using Rails optimistic locking,
    # we are going to get some exceptions instead of inconsistency.
    # So it's good idea to retry the action if it fails, and increase chances of performing action in this request
    def retry_if_locked
      @retries ||= 0

      yield
    rescue ActiveRecord::StaleObjectError => err
      if @retries < RETRIES_IF_LOCKED
        @retries += 1

        # Don't set too big retries count. It could increase response time
        delay = rand / 100
        sleep(delay)

        reload
        retry
      else
        # Log unsuccessful transaction here
        raise err
      end
    end
  end
end
