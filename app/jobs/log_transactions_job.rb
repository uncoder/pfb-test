class LogTransactionsJob < ApplicationJob
  queue_as :default

  def perform(attributes)
    Transaction.create!(attributes)
  end
end
